<?php

namespace Drupal\Tests\fixed_translation_language_interface\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;

/**
 * Test that the links are changed to the language expected.
 *
 * @group fixed_translation_language_interface
 */
class LinkAlteredTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'fixed_translation_language_interface',
    'content_translation',
    'language',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $node_type = NodeType::create([
      'type' => 'article',
      'title' => 'Article',
    ]);
    $node_type->save();
    /** @var \Drupal\content_translation\ContentTranslationManagerInterface $mng */
    $mng = $this->container->get('content_translation.manager');
    $mng->setEnabled('node', 'article', TRUE);

    // Then add another language, for example Northern Sami. The reason I chose
    // this language is because I am writing this on February 6th, which is the
    // Sami National Day. The day is also the International Day of Zero
    // Tolerance to Female Genital Mutilation, just for your information.
    ConfigurableLanguage::createFromLangcode('se')->save();
    $user = $this->createUser([
      'access content',
      'create content translations',
      'translate article node',
    ]);
    $this->drupalLogin($user);
  }

  /**
   * Test that the links we are viewing are changed to the fixed language.
   */
  public function testLinkAltered() {
    $node = $this->drupalCreateNode([
      'title' => 'test node',
      'langcode' => 'en',
      'type' => 'article',
    ]);
    $this->drupalGet($node->toUrl('drupal:content-translation-overview'));
    $link = $this->assertSession()->elementExists('css', 'table a[hreflang="se"]');
    $href = $link->getAttribute('href');
    self::assertEquals(FALSE, strpos($href, 'se/node/1'), 'The a href attribute did not start with se langcode');
    // However, it should end with the adding of the translation.
    self::assertNotEquals(FALSE, strpos($href, 'add/en/se'), 'The a href attribute points to adding a translation');
  }

}
